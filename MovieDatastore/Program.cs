using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MovieDatastore.Model;
using System.Reflection;
using MovieDatastore.Services;

namespace MovieDatastore
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Builds/runs WEB API:
            BuildWebAPI(args);

            // Makes sure console doesn't close by itself:
            Console.ReadLine();
        }

        /// <summary>
        /// Builds/runs Web API
        /// </summary>
        /// <param name="args"> Main method args </param>
        public static void BuildWebAPI(string[] args)
        {
            // Add services to the container using a builder.
            var builder = WebApplication.CreateBuilder(args);
            builder.Services.AddControllers();
            builder.Services.AddAutoMapper(typeof(Program));
            builder.Services.AddDbContext<MovieDatastoreDbContext>(options =>
            options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
            builder.Services.AddScoped(typeof(ICharacterService), typeof(CharacterService));
            builder.Services.AddScoped(typeof(IMovieService), typeof(MovieService));
            builder.Services.AddScoped(typeof(IFranchiseService), typeof(FranchiseService));

            // Add Swagger:
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "MovieDatastore API",
                    Version = "v2",
                    Description = "A simple example ASP.NET Core Web API to track characters, movies they appear in, and franchises they belong to. Made with best practices in mind as part of the Noroff Accelerate Fullstack course.",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Nicolai Enggren",
                        Url = new Uri("https://gitlab.com/nicolai_eng"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under MIT",
                        Url = new Uri("https://opensource.org/licenses/MIT"),
                    }
                });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            // Build application
            var app = builder.Build();

            // Configure the HTTP request pipeline.

            if (app.Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "MovieDatastoreAPI v1"));
            }

            app.UseHttpsRedirection();
            app.UseAuthorization();
            app.MapControllers();

            // Run Application:
            app.Run();
        }
    }
}