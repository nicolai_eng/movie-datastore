﻿using AutoMapper;
using MovieDatastore.Model.Domain;
using MovieDatastore.Model.DTO.Movie;

namespace MovieDatastore.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(dest => dest.Franchise, opt => opt
                    .MapFrom(src => src.FranchiseId))
                // Turning related movies into int arrays
                .ForMember(dest => dest.Characters, opt => opt
                    .MapFrom(src => src.Characters.Select(c => c.CharacterId).ToArray()))
                .ReverseMap();
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieEditDTO, Movie>();
        }
    }
}
