﻿using AutoMapper;
using MovieDatastore.Model.Domain;
using MovieDatastore.Model.DTO.Franchise;

namespace MovieDatastore.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                // Turning related movies into int arrays
                .ForMember(dest => dest.Movies, opt => opt
                    .MapFrom(src => src.Movies.Select(m => m.MovieId).ToList()));
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}
