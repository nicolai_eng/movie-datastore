﻿using AutoMapper;
using MovieDatastore.Model.Domain;
using MovieDatastore.Model.DTO.Character;

namespace MovieDatastore.Profiles
{
    public class CharacterProfile: Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                // Turning related movies into int arrays
                .ForMember(dest => dest.Movies, opt => opt
                    .MapFrom(src => src.Movies.Select(m => m.MovieId).ToList()));
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
