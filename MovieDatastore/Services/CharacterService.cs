﻿using Microsoft.EntityFrameworkCore;
using MovieDatastore.Model;
using MovieDatastore.Model.Domain;

namespace MovieDatastore.Services
{
    /// <summary>
    /// Character service depends on DbContext, frees up controllers to change implementation for testing.
    /// </summary>
    public class CharacterService : ICharacterService
    {
        private readonly MovieDatastoreDbContext _context;
        
        public CharacterService(MovieDatastoreDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates a new character
        /// </summary>
        /// <param name="character"></param>
        /// <returns>Newly created character</returns>
        public async Task<Character> CreateCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        /// <summary>
        /// Get all characters in database
        /// </summary>
        /// <returns>List of characters</returns>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            var characters = await _context.Characters
                .Include(c => c.Movies)
                .ToListAsync();

            return characters;
        }

        /// <summary>
        /// Get a specific character by ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Character</returns>
        public async Task<Character> GetCharacterByIdAsync(int id)
        {
            var character = await _context.Characters
                .Include(c => c.Movies)
                .FirstOrDefaultAsync(c => c.CharacterId == id);

            return character;
        }
        /// <summary>
        /// Updates a character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes a character from database by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks if character exists by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.CharacterId == id);
        }
    }
}
