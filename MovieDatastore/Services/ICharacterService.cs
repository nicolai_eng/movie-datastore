﻿using MovieDatastore.Model.Domain;

namespace MovieDatastore.Services
{
    /// <summary>
    /// Interface for CharacterService
    /// </summary>
    public interface ICharacterService
    {
        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync();
        /// <summary>
        /// Gets a character by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Character> GetCharacterByIdAsync(int id);
        /// <summary>
        /// Creates a character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task<Character> CreateCharacterAsync(Character character);
        /// <summary>
        /// Updates a character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task UpdateCharacterAsync(Character character);
        /// <summary>
        /// Deletes a character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteCharacterAsync(int id);
        /// <summary>
        /// Checks if a character exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id);
    }
}
