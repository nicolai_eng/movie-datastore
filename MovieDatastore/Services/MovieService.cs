﻿using Microsoft.EntityFrameworkCore;
using MovieDatastore.Model;
using MovieDatastore.Model.Domain;

namespace MovieDatastore.Services
{
    /// <summary>
    /// Movie service depends on DbContext, frees up controllers to change implementation for testing.
    /// </summary>
    public class MovieService : IMovieService
    {
        private readonly MovieDatastoreDbContext _context;

        public MovieService(MovieDatastoreDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates a new movie in the database.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>Newly created movie</returns>
        public async Task<Movie> CreateMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        /// <summary>
        /// Gets all movies from the database.
        /// </summary>
        /// <returns>List of movies</returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .Include(m => m.Franchise)
                .ToListAsync();
        }

        /// <summary>
        /// Gets a specific movie from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Fetched movie</returns>
        public async Task<Movie> GetMovieAsync(int id)
        {
            var movie = await _context.Movies
                .Include(m => m.Characters)
                .FirstOrDefaultAsync(m => m.MovieId == id);

            return movie;
        }
        
        /// <summary>
        /// Adds characters to a movie by ID
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public async Task AddCharactersToMovieAsync(int movieId, int[] characterIds)
        {
            // Fetches movie from database
            var movie = await _context.Movies
                .Include(m => m.Characters)
                .FirstOrDefaultAsync(m => m.MovieId == movieId);

            // If movie is not found, throws exception
            if (movie == null)
            {
                throw new ArgumentException("Movie not found");
            }

            // Fetches characters from database
            foreach (var characterId in characterIds)
            {
                var character = await _context.Characters.FindAsync(characterId);
                if (character == null)
                {
                    throw new ArgumentException("Character not found");
                }

                movie.Characters.Add(character);
            }

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets all characters belonging to a movie from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of characters</returns>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync(int id)
        {
            var characters = await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies.Any(m => m.MovieId == id))
                .ToListAsync();
            return characters;
        }

        /// <summary>
        /// Updates a movie in the database.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>Updated movie</returns>
        public async Task<Movie> UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return movie;
        }
        
        /// <summary>
        /// Deletes a movie from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies
                .FirstOrDefaultAsync(m => m.MovieId == id);

            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks if a movie exists in the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
