﻿using MovieDatastore.Model.Domain;

namespace MovieDatastore.Services
{
    /// <summary>
    /// Interface for MovieService
    /// </summary>
    public interface IMovieService
    {
        /// <summary>
        /// Gets all movies
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        /// <summary>
        /// Get move by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Movie> GetMovieAsync(int id);
        /// <summary>
        /// Create a movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task<Movie> CreateMovieAsync(Movie movie);
        /// <summary>
        /// Update a movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task<Movie> UpdateMovieAsync(Movie movie);
        /// <summary>
        /// Delete a movie
        /// </summary>
        /// <returns></returns>
        public Task DeleteMovieAsync(int id);
        /// <summary>
        /// Get all characters in a movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync(int id);
        /// <summary>
        /// Add characters to movie
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        public Task AddCharactersToMovieAsync(int movieId, int[] characterIds);

        public bool MovieExists(int movieId);


    }
}
