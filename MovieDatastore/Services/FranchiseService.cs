﻿using Microsoft.EntityFrameworkCore;
using MovieDatastore.Model;
using MovieDatastore.Model.Domain;

namespace MovieDatastore.Services
{
    /// <summary>
    /// Franchise service depends on DbContext, frees up controllers to change implementation for testing.
    /// </summary>
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDatastoreDbContext _context;
        
        public FranchiseService(MovieDatastoreDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates a new franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Newly Created Franchise</returns>
        public async Task<Franchise> CreateFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        /// <summary>
        /// Gets all franchises
        /// </summary>
        /// <returns>List of franchises</returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .ToListAsync();
        }
        
        /// <summary>
        /// Gets a franchise by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Fetched franchise</returns>
        public async Task<Franchise> GetFranchiseByIdAsync(int id)
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .FirstOrDefaultAsync(f => f.FranchiseId == id);
        }

        /// <summary>
        /// Adds movie to franchise, and saves changes to database.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        public async Task AddMoviesToFranchiseAsync(int franchiseId, int[] movieIds)
        {
            var franchise = await _context.Franchises
                .Include(f => f.Movies)
                .FirstOrDefaultAsync(f => f.FranchiseId == franchiseId);

            foreach (var movieId in movieIds)
            {
                var movie = await _context.Movies
                    .FirstOrDefaultAsync(m => m.MovieId == movieId);

                franchise.Movies.Add(movie);
            }

            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Deletes a franchise by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises
                .FirstOrDefaultAsync(f => f.FranchiseId == id);

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets all characters in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>List of characters</returns>
        public async Task<IEnumerable<Character>> GetAllCharactersInFranchiseAsync(int franchiseId)
        {

            var characters = await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies.Any(m => m.FranchiseId == franchiseId))
                .ToListAsync();

            return characters;
        }

        /// <summary>
        /// Gets all movies in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>List of movies</returns>
        public async Task<IEnumerable<Movie>> GetMoviesInFranchiseAsync(int franchiseId)
        {
            var franchise = await _context.Franchises
                .Include(f => f.Movies)
                .ThenInclude(m => m.Characters)
                .FirstOrDefaultAsync(f => f.FranchiseId == franchiseId);

            return franchise.Movies;
        }

        /// <summary>
        /// Updates a franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>Updated Franchise</returns>
        public async Task<Franchise> UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return franchise;
        }

        /// <summary>
        /// Checks if a franchise exists by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(c => c.FranchiseId == id);
        }
    }
}
