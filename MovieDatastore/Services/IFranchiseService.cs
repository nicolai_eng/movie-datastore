﻿using MovieDatastore.Model.Domain;

namespace MovieDatastore.Services
{
    /// <summary>
    /// Interface for FranchiseService
    /// </summary>
    public interface IFranchiseService
    {
        /// <summary>
        /// Gets all franchises
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        /// <summary>
        /// Gets a franchise by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Franchise> GetFranchiseByIdAsync(int id);
        /// <summary>
        /// Creates a franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task<Franchise> CreateFranchiseAsync(Franchise franchise);
        /// <summary>
        /// Updates a franchise
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task<Franchise> UpdateFranchiseAsync(Franchise franchise);
        /// <summary>
        /// Deletes a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteFranchiseAsync(int id);
        /// <summary>
        /// Checks if franchise exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool FranchiseExists(int id);
        /// <summary>
        /// Gets all movies in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetMoviesInFranchiseAsync(int franchiseId);
        /// <summary>
        /// Gets all characters in a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersInFranchiseAsync(int franchiseId);
        /// <summary>
        /// Adds movies to a franchise
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movieIds"></param>
        /// <returns></returns>
        public Task AddMoviesToFranchiseAsync(int franchiseId, int[] movieIds);
    }
}
