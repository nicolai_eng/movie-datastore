﻿using Microsoft.EntityFrameworkCore;
using MovieDatastore.Model.Domain;

namespace MovieDatastore.Model
{
    /// <summary>
    /// Class that builds database context
    /// </summary>
    public class MovieDatastoreDbContext : DbContext
    {
        // Creates three tables in database
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        /// <summary>
        /// Movie Data Store Database Context Constructor
        /// </summary>
        /// <param name="options"></param>
        /// <inheritdoc/>
        public MovieDatastoreDbContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Seed franchise, character, and movie data.
        /// </summary>
        /// <inheritdoc></inheritdoc>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Prepare data
            #region Franchises
            var lordOfTheRingsFranchise = new Franchise()
            {
                FranchiseId = 1,
                Name = "Lord of the Rings",
                Description =
                    "The Lord of the Rings is an epic high fantasy novel written by English author and scholar J. R. R. Tolkien. " +
                    "The story began as a sequel to Tolkien's 1937 fantasy novel The Hobbit, but eventually developed into a much larger work. " +
                    "Written in stages between 1937 and 1949, The Lord of the Rings is one of the best-selling novels ever written, with over 150 million copies sold.",
            };
            var jamesBondFranchise = new Franchise()
            {
                FranchiseId = 2,
                Name = "James Bond",
                Description =
                    "James Bond is a fictional character created by the British journalist and novelist Ian Fleming in 1953. " +
                    "He is the protagonist of the James Bond series of novels, films, comics and video games. " +
                    "Bond is an intelligence officer in the Secret Intelligence Service, commonly known as MI6. " +
                    "He is also known by his code number, 007, and was a Royal Naval Reserve Commander. " +
                    "Bond is known for his preference for expensive suits, fast cars, and exotic holidays. " +
                    "He is also known for his love of the British countryside, his dry martini, and his love",
            };
            var harryPotterFranchise = new Franchise()
            {
                FranchiseId = 3,
                Name = "Harry Potter",
                Description =
                    "Harry Potter is a series of fantasy novels written by British author J. K. Rowling. " +
                    "The novels chronicle the life of a young wizard, Harry Potter, and his friends Hermione Granger and Ron Weasley, " +
                    "all of whom are students at Hogwarts School of Witchcraft and Wizardry. " +
                    "The main story arc concerns Harry's struggle against Lord Voldemort, a dark wizard who intends to become immortal, " +
                    "overthrow the wizard governing body known as the Ministry of Magic, and subjugate all wizards and muggles, a reference to non-magical people.",
            };
            #endregion
            #region Characters
            var frodoBaggins = new Character()
            {
                CharacterId = 1,
                FullName = "Frodo Baggins",
                Alias = "Ring-bearer",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/4/4e/Elijah_Wood_as_Frodo_Baggins.png/" +
                          "170px-Elijah_Wood_as_Frodo_Baggins.png"
            };
            var gandalf = new Character()
            {
                CharacterId = 2,
                FullName = "Gandalf",
                Alias = "The Grey/White",
                Gender = "Male",
                Picture = "https://static.wikia.nocookie.net/lotr/images/8/8d/Gandalf-2.jpg/revision/latest?cb=20130209172436"
            };
            var jamesBond = new Character()
            {
                CharacterId = 3,
                FullName = "James Bond",
                Alias = "Agent 007",
                Gender = "Male",
                Picture = "https://images.immediate.co.uk/production/volatile/sites/3/2021/09/daniel-craig-007.jpg-303a730.png"
            };
            var harryPotter = new Character()
            {
                CharacterId = 4,
                FullName = "Harry Potter",
                Alias = "The Boy who Lived",
                Gender = "Male",
                Picture = "https://upload.wikimedia.org/wikipedia/en/d/d7/Harry_Potter_character_poster.jpg"
            };
            #endregion
            #region Movies
            var fellowshipOfRing = new Movie()
            {
                MovieId = 1,
                MovieTitle = "The Fellowship of The Ring",
                Genre = "Fantasy",
                ReleaseYear = "2001",
                Director = "Peter Jackson",
                Picture = "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.imdb." +
                "com%2Ftitle%2Ftt0120737%2F&psig=AOvVaw1b9Q5A0jlE8R8PCyqH8S51&ust=" +
                "1664212317295000&source=images&cd=vfe&ved=0CAwQjRxqFwoTCNDk0JO4sPoCFQAAAAAdAAAAABAD",
                Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4&ab_channel=Movieclips",
                FranchiseId = 1,
            };

            var casinoRoyale = new Movie()
            {
                MovieId = 2,
                MovieTitle = "Casino Royale",
                Genre = "Action",
                ReleaseYear = "2006",
                Director = "Martin Campbell",
                Picture = "https://www.imdb.com/title/tt0381061/mediaviewer/rm119363072/?ref_=tt_ov_i",
                Trailer = "https://www.youtube.com/watch?v=36mnx8dBbGE&ab_channel=MOVIECLIPSNews",
                FranchiseId = 2
            };

            var harryPotterAndThePhilosophersStone = new Movie()
            {
                MovieId = 3,
                MovieTitle = "Harry Potter and the Philosopher's Stone",
                Genre = "Fantasy",
                ReleaseYear = "2001",
                Director = "Chris Columbus",
                Picture = "https://www.imdb.com/title/tt0241527/mediaviewer/rm119363072/?ref_=tt_ov_i",
                Trailer = "https://www.youtube.com/watch?v=VyHV0BRtdxo&ab_channel=Movieclips",
                FranchiseId = 3,
            };

            var deathlyHallows = new Movie()
            {
                MovieId = 4,
                MovieTitle = "The Deathly Hallows",
                Genre = "Fantasy",
                ReleaseYear = "2010",
                Director = "David Yates",
                Picture = "https://www.imdb.com/title/tt0926084/mediaviewer/rm706774528/?ref_=tt_ov_i",
                Trailer = "https://www.youtube.com/watch?v=MxqsmsA8y5k&ab_channel=Movieclips",
                FranchiseId = 3,
            };
            #endregion

            // Seed data
            modelBuilder.Entity<Franchise>().HasData(lordOfTheRingsFranchise);
            modelBuilder.Entity<Franchise>().HasData(jamesBondFranchise);
            modelBuilder.Entity<Franchise>().HasData(harryPotterFranchise);

            modelBuilder.Entity<Character>().HasData(frodoBaggins);
            modelBuilder.Entity<Character>().HasData(gandalf);
            modelBuilder.Entity<Character>().HasData(jamesBond);
            modelBuilder.Entity<Character>().HasData(harryPotter);

            modelBuilder.Entity<Movie>().HasData(fellowshipOfRing);
            modelBuilder.Entity<Movie>().HasData(casinoRoyale);
            modelBuilder.Entity<Movie>().HasData(harryPotterAndThePhilosophersStone);
            modelBuilder.Entity<Movie>().HasData(deathlyHallows);

            // Seed m2m movie-character. Need to define m2m and access linking table
            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Characters)
                .WithMany(c => c.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    je =>
                    {
                        je.HasKey("MovieId", "CharacterId");
                        je.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 3 },
                            new { MovieId = 3, CharacterId = 4 },
                            new { MovieId = 4, CharacterId = 4 }
                        );
                    });

        }
    }
}
