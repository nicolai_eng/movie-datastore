﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieDatastore.Model.Domain
{
    /// <summary>
    /// Character data model used for creating Character-table in database
    /// </summary>
    [Table("Character")]
    public class Character
    {
        /// <summary>
        /// CharacterId is the primary key for the Character-table
        /// </summary>
        /// <example>99</example>
        [Key]
        public int CharacterId { get; set; }
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        [MaxLength(50)]
        public string Alias { get; set; }
        [MaxLength(50)]
        public string Gender { get; set; }
        [Url]
        public string Picture { get; set; } // URL to picture

        // Navigates to "Movie" model. Can be related to many movies.
        public ICollection<Movie> Movies { get; set; }
    }
}
