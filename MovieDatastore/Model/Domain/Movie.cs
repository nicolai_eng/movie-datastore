﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieDatastore.Model.Domain
{
    /// <summary>
    /// Movie data model used for creating Movie-table in database
    /// </summary>
    [Table("Movie")]
    public class Movie
    {
        [Key]
        [Required]
        public int MovieId { get; set; }
        [MaxLength(100)]
        public string MovieTitle { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
        [MaxLength(4)]
        public string ReleaseYear { get; set; }
        [MaxLength(100)]
        public string Director { get; set; }
        [Url]
        public string Picture { get; set; } // URL to picture
        [Url]
        public string Trailer { get; set; } // Youtube link

        // Navigates to model "character". Can be related to many characters.
        public ICollection<Character> Characters { get; set; }

        // Navigates to principle model "franchise";
        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
    }
}
