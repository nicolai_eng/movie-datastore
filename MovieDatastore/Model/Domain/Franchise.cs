﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieDatastore.Model.Domain
{
    /// <summary>
    /// Franchise data model used for creating Franchise-table in database
    /// </summary>
    [Table("Franchise")]
    public class Franchise
    {
        [Key]
        [Required]
        public int FranchiseId { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(800)]
        public string Description { get; set; }

        // Navigates to dependent model "Movie". Can be related to many movies.
        public ICollection<Movie> Movies { get; set; }

    }
}
