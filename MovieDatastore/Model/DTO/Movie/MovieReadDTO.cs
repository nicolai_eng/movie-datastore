﻿namespace MovieDatastore.Model.DTO.Movie
{
    public class MovieReadDTO
    {
        /// <summary>
        /// MovieId is the primary key for the Franchise-table in the database.
        /// </summary>
        /// <example>1</example>
        public int MovieId { get; set; }
        /// <summary>
        /// Title of the movie.
        /// </summary>
        /// <example>The Fellowship of The Ring</example>
        public string MovieTitle { get; set; }
        /// <summary>
        /// Genre of the movie.
        /// </summary>
        /// <example>Fantasy</example>
        public string Genre { get; set; }
        /// <summary>
        /// Release year of the movie.
        /// </summary>
        /// <example>2001</example>
        public string ReleaseYear { get; set; }
        /// <summary>
        /// Director of the movie.
        /// </summary>
        /// <example>Peter Jackson</example>
        public string Director { get; set; }

#pragma warning disable CS1570 // XML comment has badly formed XML
        /// <summary>
        /// URL string to picture/poster of the movie.
        /// </summary>
        /// <example>https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.imdb.com%2Ftitle%2Ftt0120737%2F&psig=AOvVaw1b9Q5A0jlE8R8PCyqH8S51&ust=1664212317295000
        /// &source=images&cd=vfe&ved=0CAwQjRxqFwoTCNDk0JO4sPoCFQAAAAAdAAAAABAD
        /// </example>
#pragma warning restore CS1570 // XML comment has badly formed XML
        public string Picture { get; set; }
        /// <summary>
        /// URL string to trailer of the movie.
        /// </summary>
        /// <example>https://www.youtube.com/watch?v=V75dMMIW2B4</example>
        public string Trailer { get; set; }
        /// <summary>
        /// Franchise of the movie by ID.
        /// </summary>
        /// <example>1</example>
        public int Franchise { get; set; }
        /// <summary>
        /// Characters in the movie by ID.
        /// </summary>
        /// <example>[1
        /// <para>1</para>
        /// <para>2</para>
        /// ]</example>
        public List<int> Characters { get; set; }
    }
}
