﻿using System.ComponentModel.DataAnnotations;

namespace MovieDatastore.Model.DTO.Movie
{
    public class MovieEditDTO
    {
        /// <summary>
        /// Title of the movie.
        /// </summary>
        /// <example>The Fellowship of The Ring</example>
        [MaxLength(100)]
        public string MovieTitle { get; set; }
        /// <summary>
        /// Genre of the movie.
        /// </summary>
        /// <example>Fantasy</example>
        [MaxLength(50)]
        public string Genre { get; set; }
        /// <summary>
        /// Release year of the movie.
        /// </summary>
        /// <example>2001</example>
        [MaxLength(4)]
        public string ReleaseYear { get; set; }
        /// <summary>
        /// Director of the movie.
        /// </summary>
        /// <example>Peter Jackson</example>
        [MaxLength(100)]
        public string Director { get; set; }
#pragma warning disable CS1570 // XML comment has badly formed XML
        /// <summary>
        /// URL string to picture/poster of the movie.
        /// </summary>
        /// <example>https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.imdb.com%2Ftitle%2Ftt0120737%2F&psig=AOvVaw1b9Q5A0jlE8R8PCyqH8S51&ust=1664212317295000
        /// &source=images&cd=vfe&ved=0CAwQjRxqFwoTCNDk0JO4sPoCFQAAAAAdAAAAABAD
        /// </example>
#pragma warning restore CS1570 // XML comment has badly formed XML
        [Url]
        public string Picture { get; set; }
        /// <summary>
        /// URL string to trailer of the movie.
        /// </summary>
        /// <example>https://www.youtube.com/watch?v=V75dMMIW2B4</example>
        [Url]
        public string Trailer { get; set; }

    }
}
