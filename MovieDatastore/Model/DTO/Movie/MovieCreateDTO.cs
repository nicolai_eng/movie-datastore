﻿namespace MovieDatastore.Model.DTO.Movie
{
    public class MovieCreateDTO
    {
        /// <summary>
        /// Title of the movie.
        /// </summary>
        /// <example>Captain America: The First Avenger</example>
        public string MovieTitle { get; set; }
        /// <summary>
        /// Genre of the movie.
        /// </summary>
        /// <example>Action</example>
        public string Genre { get; set; }
        /// <summary>
        /// Release year of the movie.
        /// </summary>
        /// <example>2011</example>
        public string ReleaseYear { get; set; }
        /// <summary>
        /// Director of the movie.
        /// </summary>
        /// <example>Joe Johnston</example>
        public string Director { get; set; }
        /// <summary>
        /// URL string to picture/poster of the movie.
        /// </summary>
        /// <example>https://m.media-amazon.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_FMjpg_UX1000_.jpg</example>
        public string Picture { get; set; }
        /// <summary>
        /// URL string to trailer of the movie.
        /// </summary>
        /// <example>https://www.youtube.com/watch?v=JerVrbLldXw</example>
        public string Trailer { get; set; }
    }
}
