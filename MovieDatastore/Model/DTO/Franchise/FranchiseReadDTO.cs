﻿namespace MovieDatastore.Model.DTO.Franchise
{
    public class FranchiseReadDTO
    {
        /// <summary>
        /// FranchiseId is the primary key for the Franchise-table in the database.
        /// </summary>
        /// <example>1</example>
        public int FranchiseId { get; set; }
        /// <summary>
        /// Name of the franchise.
        /// </summary>
        /// <example>Lord of The Rings</example>
        public string Name { get; set; }
        /// <summary>
        /// Description of the franchise.
        /// </summary>
        /// <example>
        /// <param>The Lord of the Rings is an epic high fantasy novel written by English author and scholar J. R. R. Tolkien.</param>
        /// <param>The story began as a sequel to Tolkien's 1937 fantasy novel The Hobbit, but eventually developed into a much larger work.</param>
        /// <param>Written in stages between 1937 and 1949, The Lord of the Rings is one of the best-selling novels ever written, with over 150 million copies sold.</param>
        /// </example>
        public string Description { get; set; }
        /// <summary>
        /// List of movies by ID in the franchise.
        /// </summary>
        /// <example>[
        /// 1
        /// ]</example>
        public List<int> Movies { get; set; }
    }
}
