﻿using System.ComponentModel.DataAnnotations;

namespace MovieDatastore.Model.DTO.Franchise
{
    public class FranchiseCreateDTO
    {
        /// <summary>
        /// Name of the franchise
        /// </summary>
        /// <example>Marvel Cinematic Universe</example>
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        /// <summary>
        /// Description of the franchise
        /// </summary>
        /// <example>
        /// <para>The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero</para>
        /// <para>films produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics.</para>
        /// <para>The franchise also includes television series, short films, digital series, and literature. The shared universe,</para>
        /// <para>much like the original Marvel Universe in comic books, was established by crossing over common plot elements,</para>
        /// <para>settings, cast, and characters.</para>
        /// </example>
        [MaxLength(800)]
        public string Description { get; set; }
    }
}
