﻿using System.ComponentModel.DataAnnotations;

namespace MovieDatastore.Model.DTO.Character
{
    public class CharacterCreateDTO
    {
        /// <summary>
        /// Name of the character.
        /// </summary>
        /// <example>Steve Rogers</example>
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        /// <summary>
        /// Alias of the character.
        /// </summary>
        /// <example>Captain America</example>
        [MaxLength(50)]
        public string Alias { get; set; }
        /// <summary>
        /// Gender of the character.
        /// </summary>
        /// <example>Male</example>
        [MaxLength(50)]
        public string Gender { get; set; }
        /// <summary>
        /// URL string of character picture.
        /// </summary>
        /// <example>https://static.wikia.nocookie.net/marvelcinematicuniverse/images/d/d7/CapAmerica-EndgameProfile.jpg</example>
        [Url]
        public string Picture { get; set; }
    }
}
