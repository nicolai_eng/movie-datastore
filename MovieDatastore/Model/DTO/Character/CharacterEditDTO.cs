﻿using System.ComponentModel.DataAnnotations;

namespace MovieDatastore.Model.DTO.Character
{
    public class CharacterEditDTO
    {
        /// <summary>
        /// Name of the character.
        /// </summary>
        /// <example>James Bond</example>
        [MaxLength(50)]
        public string FullName { get; set; }
        /// <summary>
        /// Alias of the character.
        /// </summary>
        /// <example>Agent 007</example>
        [MaxLength(50)]
        public string Alias { get; set; }
        /// <summary>
        /// Gender of the character.
        /// </summary>
        /// <example>Male</example>
        [MaxLength(50)]
        public string Gender { get; set; }
        /// <summary>
        /// URL string of character picture.
        /// </summary>
        /// <example>https://images.immediate.co.uk/production/volatile/sites/3/2021/09/daniel-craig-007.jpg-303a730.png</example>
        [Url]
        public string Picture { get; set; }
    }
}
