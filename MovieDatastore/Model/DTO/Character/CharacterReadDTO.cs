﻿namespace MovieDatastore.Model.DTO.Character
{
    public class CharacterReadDTO
    {
        /// <summary>
        /// CharacterId is the primary key for the Character-table in the database.
        /// </summary>
        /// <example>3</example>
        public int CharacterId { get; set; }
        /// <summary>
        /// Name of the character.
        /// </summary>
        /// <example>James Bond</example>
        public string FullName { get; set; }
        /// <summary>
        /// Alias of the character.
        /// </summary>
        /// <example>Agent 007</example>
        public string Alias { get; set; }
        /// <summary>
        /// Gender of the character.
        /// </summary>
        /// <example>Male</example>
        public string Gender { get; set; }
        /// <summary>
        /// URL string of character picture.
        /// </summary>
        /// <example>https://images.immediate.co.uk/production/volatile/sites/3/2021/09/daniel-craig-007.jpg-303a730.png</example>
        public string Picture { get; set; }
        /// <summary>
        /// List of movies the character is in by id.
        /// </summary>
        /// <example>[
        /// 2
        /// ]</example>
        public List<int> Movies { get; set; }
    }
}
