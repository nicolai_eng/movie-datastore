﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieDatastore.Model;
using MovieDatastore.Model.Domain;
using MovieDatastore.Model.DTO.Character;
using System.Net.Mime;
using MovieDatastore.Services;

namespace MovieDatastore.Controllers
{
    /// <summary>
    /// Controller for character related endpoints
    /// </summary>
    [Route("api/v2/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        // Dependency injection of database context and automapper:
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;
        public CharactersController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Adds a new character to the database.
        /// </summary>
        /// <param name="dtoCharacter"> Character for creation </param>
        /// <returns>Request status code </returns>
        [HttpPost]
        public async Task<ActionResult<CharacterCreateDTO>> PostCharacter([FromBody] CharacterCreateDTO dtoCharacter)
        {
            var domainCharacter = _mapper.Map<Character>(dtoCharacter);
            try
            {
                // Adds character to context, and saves.
                domainCharacter = await _characterService.CreateCharacterAsync(domainCharacter);

            }
            catch
            {
                // Returns error code if an exception is thrown.
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // Returns newly posted character.
            return CreatedAtAction("GetCharacter", new { id = domainCharacter.CharacterId }, dtoCharacter);
        }

        /// <summary>
        /// Fetches all characters in the database.
        /// </summary>
        /// <returns>List of all characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());
        }

        /// <summary>
        /// Fetches a specific character by ID.
        /// </summary>
        /// <param name="id"> id of character </param>
        /// <returns> Character </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            // Finds character by Id
            var character = await _characterService.GetCharacterByIdAsync(id);

            // If no character was found, a Not Found code is returned.
            if (character == null) return NotFound();

            return _mapper.Map<CharacterReadDTO>(character); // Returns character.
        }

        /// <summary>
        /// Updates a character in the database. Must pass full character object and ID in route.
        /// </summary>
        /// <param name="id"> id of character </param>
        /// <param name="dtoCharacter"> Character to update </param>
        /// <returns> Request status code </returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            var domainCharacter = _mapper.Map<Character>(dtoCharacter);
            domainCharacter.CharacterId = id;

            try
            {
                // Updates and saves character.
                await _characterService.UpdateCharacterAsync(domainCharacter);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent(); // If success, no code is returned.
        }

        /// <summary>
        /// Deletes a character from the database by ID.
        /// </summary>
        /// <param name="id"> Id of character </param>
        /// <returns> Request status code </returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            // Finds character from Id.
            var character = await _characterService.GetCharacterByIdAsync(id);

            // If no character was found, a Not Found code is returned.
            if (character == null) return NotFound();

            try
            {
                // Character is removed, and context is updated.
                await _characterService.DeleteCharacterAsync(character.CharacterId);
            }
            catch (Exception ex)
            {
                // If exception occurs, a Bad Request is returned.
                return BadRequest(ex.GetType());
            }

            return NoContent(); // If success, no code is returned.
        }

        /// <summary>
        /// Checks if character exists by ID.
        /// </summary>
        /// <param name="id">id used to check if character exists</param>
        /// <returns>True or False</returns>
        private bool CharacterExists(int id)
        {
            return _characterService.CharacterExists(id);
        }
    }
}
