using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieDatastore.Model;
using MovieDatastore.Model.Domain;
using MovieDatastore.Model.DTO.Character;
using MovieDatastore.Model.DTO.Movie;
using System.Net.Mime;
using MovieDatastore.Services;

namespace MovieDatastore.Controllers
{
    /// <summary>
    /// Controller for movie related endpoints
    /// </summary>
    [Route("api/v2/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        // Dependency injection of movie service and automapper:
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Adds a new movie to the database.
        /// </summary>
        /// <param name="dtoMovie"> Movie object to be inserted </param>
        /// <returns>Request status code </returns>
        [HttpPost]
        public async Task<ActionResult<MovieCreateDTO>> PostMovie([FromBody] MovieCreateDTO dtoMovie)
        {
            var domainMovie = _mapper.Map<Movie>(dtoMovie);
            try
            {
                // Adds movie to context, and saves.
                domainMovie = await _movieService.CreateMovieAsync(domainMovie);
            }
            catch
            {
                // Returns error code if an exception is thrown.
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // Returns newly posted movie.
            return CreatedAtAction("GetMovie", new { id = domainMovie.MovieId }, dtoMovie);
        }

        /// <summary>
        /// Fetches all movies in the database.
        /// </summary>
        /// <returns> List of all movies </returns>
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var domainMovies = await _movieService.GetAllMoviesAsync();
            var dtoMovies = _mapper.Map<IEnumerable<MovieReadDTO>>(domainMovies);
            return Ok(dtoMovies);
        }

        /// <summary>
        /// Fetches a specific movie by ID.
        /// </summary>
        /// <param name="id"> id of movie </param>
        /// <returns> Movie </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var domainMovie = await _movieService.GetMovieAsync(id);
            if (domainMovie == null)
            {
                return NotFound();
            }
            var dtoMovie = _mapper.Map<MovieReadDTO>(domainMovie);
            return Ok(dtoMovie);
        }

        /// <summary>
        /// Fetches all characters from a specific movie, by movie ID.
        /// </summary>
        /// <param name="id"> Id of movie </param>
        /// <returns> List of characters </returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters(int id)
        {
            var domainCharacters = await _movieService.GetAllCharactersAsync(id);
            var dtoCharacters = _mapper.Map<IEnumerable<CharacterReadDTO>>(domainCharacters);
            return Ok(dtoCharacters);
        }

        /// <summary>
        /// Updates a movie in the database. Must pass full movie object.
        /// </summary>
        /// <param name="id"> ID of movie </param>
        /// <param name="dtoMovie"> Movie object for update </param>
        /// <returns> Request status code</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutMovie(int id, [FromBody] MovieEditDTO dtoMovie)
        {
            var domainMovie = _mapper.Map<Movie>(dtoMovie);
            domainMovie.MovieId = id;
            try
            {
                await _movieService.UpdateMovieAsync(domainMovie);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Add characters by ID to a movie with movie ID.
        /// </summary>
        /// <param name="id"> Id of movie </param>
        /// <param name="characterIds"> Id list of characters added to movie</param>
        /// <returns> Request status code </returns>
        [HttpPut("{id}/characters")]
        public async Task<ActionResult> AddMovieCharacters(int id, [FromBody] int[] characterIds)
        {
            try
            {
                await _movieService.AddCharactersToMovieAsync(id, characterIds);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Deletes a movie from the database by ID.
        /// </summary>
        /// <param name="id"> Id of movie </param>
        /// <returns> Request status code </returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovie(int id)
        {
            var domainMovie = await _movieService.GetMovieAsync(id);
            if (domainMovie == null)
            {
                return NotFound();
            }
            await _movieService.DeleteMovieAsync(domainMovie.MovieId);
            return NoContent();
        }

        /// <summary>
        /// Checks if a movie exists in the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool MovieExists(int id)
        {
            return _movieService.MovieExists(id);
        }
    }
}