﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieDatastore.Model;
using MovieDatastore.Model.Domain;
using MovieDatastore.Model.DTO.Character;
using MovieDatastore.Model.DTO.Franchise;
using MovieDatastore.Model.DTO.Movie;
using System.Net.Mime;
using MovieDatastore.Services;

namespace MovieDatastore.Controllers
{
    /// <summary>
    /// Controller for franchise related endpoints
    /// </summary>
    [Route("api/v2/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        // Dependency injection of franchise service and automapper:
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Adds a new franchise to the database.
        /// </summary>
        /// <param name="dtoFranchise"> Franchise object to be added </param>
        /// <returns> Request status code </returns>
        [HttpPost]
        public async Task<ActionResult<FranchiseCreateDTO>> PostFranchise([FromBody] FranchiseCreateDTO dtoFranchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            try
            {
                // Adds franchise to context, and saves.
                domainFranchise = await _franchiseService.CreateFranchiseAsync(domainFranchise);
            }
            catch
            {
                // Returns error code if an exception is thrown.
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            // Returns newly posted franchise.
            return CreatedAtAction("GetFranchise", new { id = domainFranchise.FranchiseId }, dtoFranchise);
        }
        
        /// <summary>
        /// Fetches all franchises in the database.
        /// </summary>
        /// <returns>List of all franchises</returns>
        [HttpGet()]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchises = await _franchiseService.GetAllFranchisesAsync();
            var dtoFranchises = _mapper.Map<IEnumerable<FranchiseReadDTO>>(franchises);
            return Ok(dtoFranchises);
        }
        
        /// <summary>
        /// Fetches a single franchise by ID.
        /// </summary>
        /// <param name="id"> Id of franchise </param>
        /// <returns> Request status code </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetFranchiseByIdAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            var dtoFranchise = _mapper.Map<FranchiseReadDTO>(franchise);
            return Ok(dtoFranchise);
        }

        /// <summary>
        /// Fetches all movies in a specific franchise, by franchise ID.
        /// </summary>
        /// <param name="id"> Id of franchise </param>
        /// <returns> List of movies </returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies(int id)
        {
            var franchise = await _franchiseService.GetFranchiseByIdAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            var movies = await _franchiseService.GetMoviesInFranchiseAsync(id);
            var dtoMovies = _mapper.Map<IEnumerable<MovieReadDTO>>(movies);
            return Ok(dtoMovies);
        }

        /// <summary>
        /// Fetches all characters in a specific franchise, by franchise ID.
        /// </summary>
        /// <param name="id"> Id of franchise </param>
        /// <returns> List of characters </returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters(int id)
        {
            var franchise = await _franchiseService.GetFranchiseByIdAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            var characters = await _franchiseService.GetAllCharactersInFranchiseAsync(id);
            var dtoCharacters = _mapper.Map<IEnumerable<CharacterReadDTO>>(characters);
            return Ok(dtoCharacters);
        }

        /// <summary>
        /// Updates a franchise. Must pass full franchise object and franchise ID in route.
        /// </summary>
        /// <param name="id"> Id of franchise </param>
        /// <param name="dtoFranchise"> Franchise object to use for update </param>
        /// <returns> Request status code </returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            var domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            domainFranchise.FranchiseId = id;

            try
            {
                // Updates and saves character.
                await _franchiseService.UpdateFranchiseAsync(domainFranchise);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent(); // If success, no code is returned. 
        }

        /// <summary>
        /// Add movies by ID to franchise with franchise ID. Will override existing movie-franchise relationship.
        /// </summary>
        /// <param name="id"> Id of franchise </param>
        /// <param name="movieIds"> List of Movie Ids to be added to franchise </param>
        /// <returns> Status code of request </returns>
        [HttpPut("{id}/movies")]
        public async Task<ActionResult> AddFranchiseMovies(int id, int[] movieIds)
        {
            try
            {
                await _franchiseService.AddMoviesToFranchiseAsync(id, movieIds);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Delete franchise from database by ID.
        /// </summary>
        /// <param name="id"> Id of franchise to be deleted </param>
        /// <returns> Request status code </returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteFranchise(int id)
        {
            var franchise = await _franchiseService.GetFranchiseByIdAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            await _franchiseService.DeleteFranchiseAsync(franchise.FranchiseId);
            return NoContent();
        }

        /// <summary>
        /// Checks if franchise exists in database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool FranchiseExists(int id)
        {
            return _franchiseService.FranchiseExists(id);
        }
    }
}
