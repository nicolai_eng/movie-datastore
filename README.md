# MOVIE-DATASTORE-API
SQL-Database created with Entity Framework Code First workflow and an ASP.NET Core Web API in C# to showcase competences regarding Web Server Development in ASP.NET Core.

## Setup SQL-Database
![MovieDataStore_database_image](MovieDataStoreDBDiagram.jpg)

The SQL Client and Web API work with a database called MovieDataStore. To create the database, open the Visual Studio Solution.
Inside appsettings.json, set the default connection inside connection strings to:
>"Data Source= **NAME/ADRESS OF THE INSTANCE OF YOUR SQL SERVER**; Initial Catalog= MovieDatastore; Integrated Security=True;"

*i.e. DESKTOP-TOOPDI6\SQLEXPRESS*

In the Package Manager Console type:
`add-migration InitialDb`

Then type 
`update-database`

## WEB API
The API uses swagger as the default launchUrl, so simply run MovieDataStore which should open a new browser window.
Here you can play around with the API.

### Try it out (some examples)
*For ease of use, these samples should already be available as examples in swagger.*

1. Add a character:
```
{
  "fullName": "Steve Rogers",
  "alias": "Captain America",
  "gender": "Male",
  "picture": "https://static.wikia.nocookie.net/marvelcinematicuniverse/images/d/d7/CapAmerica-EndgameProfile.jpg"
}
```

2. Add a movie:
```
{
  "movieTitle": "Captain America: The First Avenger",
  "genre": "Action",
  "releaseYear": "2011",
  "director": "Joe Johnston",
  "picture": "https://m.media-amazon.com/images/M/MV5BMTYzOTc2NzU3N15BMl5BanBnXkFtZTcwNjY3MDE3NQ@@._V1_FMjpg_UX1000_.jpg",
  "trailer": "https://www.youtube.com/watch?v=JerVrbLldXw"
}
```

3. Add our new character (id should be id 5) to our new movie (id should be 5):
```
[
  5
]
```

4. Add a new franchise
```
{
  "name": "Marvel Cinematic Universe",
  "description": "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superherofilms produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics.The franchise also includes television series, short films, digital series, and literature. The shared universe,much like the original Marvel Universe in comic books, was established by crossing over common plot elements,settings, cast, and characters."
}
```

5. Add movie (id should be 5) to franchise (id should be 5)
```
[
  5
]
```

6. Try to fetch characters from movies and franchices, edit entries and play around...!

## Contributors

-   [Oliver Engermann (@OliverEng)](@OliverEng)
-   [Nicolai Krumhardt Enggren (@nicolai_eng)](@nicolai_eng)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

Noroff Accelerate, 2022.
